#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DETAIL_DB	"details.db"

typedef struct details{
	int id;
	int price;
	int quantity;
	char name [20]; 
}details_t;

void item_display(details_t *b) {
	printf("%d, %.2lf,  %d, %s\n", b->id, b->price, b->quantity, b->name);
}

void item_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	details_t b;
	fp = fopen(DETAIL_DB, "rb");
	if(fp == NULL) {
		perror("failed to open item file");
		return;
	}
	
	while(fread(&b, sizeof(details_t), 1, fp) > 0) {
		if(strstr(b.name, name) != NULL) {
			found = 1;
			item_display(&b);
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such item found.\n");
}






void detail_accept(details_t *b) {
	printf("id: ");
	scanf("%d", &b->id);
	printf("price: ");
	scanf("%lf", &b->price);
	printf("quantity: ");
	scanf("%d", &b->quantity);
	printf("name: ");
	scanf("%s", b->name);
}

int get_next_detail_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(details_t);
	details_t u;
	fp = fopen(DETAIL_DB, "rb");
	if(fp == NULL)
		return max + 1;
	fseek(fp, -size, SEEK_END);
	if(fread(&u, size, 1, fp) > 0)
		max = u.id;
	fclose(fp);
	return max + 1;
}

void add()
{
	FILE *fp;
	details_t b;
	detail_accept(&b);
	b.id = get_next_detail_id();
	fp = fopen(DETAIL_DB, "ab");
	if(fp == NULL) {
		perror("cannot open file");
		exit(1);
	}
	fwrite(&b, sizeof(details_t), 1, fp);
	printf("item added in file.\n");
	fclose(fp);
}




void find(){
	char name [20];
	printf("Enter item name: ");
	scanf("%s",&name);
	item_find_by_name(name);
}

void display_all()

{
	
}

void edit()
{
	int id, found = 0;
	FILE *fp;
	details_t u;
	printf("enter id: ");
	scanf("%d", &id);
	fp = fopen(DETAIL_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open item file");
		exit(1);
	}
	while(fread(&u, sizeof(details_t), 1, fp) > 0) {
		if(id == u.id) {
			found = 1;
			break;
		}
	}
	if(found) {
		long size = sizeof(details_t);
		details_t nb;
		detail_accept(&nb);
		nb.id = u.id;
		fseek(fp, -size, SEEK_CUR);
		fwrite(&nb, size, 1, fp);
		printf("item updated.\n");
	}
	else 
		printf("item not found.\n");
	fclose(fp);
		
}

void delete()
{
	FILE *fp;
	FILE *fp_tmp;
	int found=0;
	details_t u;
}

void main()
{
	int ch =0;
	while(1)
	{
	printf("\nchoose the operation >>");
	printf("\n1-add\n \n2-find \n\n3-display_all\n\n4-edit\n\n5-delete\n");
	scanf("%d",&ch);
	switch(ch)
	{
		case 1 : add(); 
              break ;

		case 2 : find(); 
              break;

		case 3 : display_all(); 
              break;

		case 4 : edit(); 
              break;
			  
		case 5 : delete(); 
              break;
			  }
}
			  
 }
