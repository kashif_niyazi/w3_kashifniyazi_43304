#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
  
// struct person with 3 fields 
struct Books { 
    char* name; 
    int id; 
    char price; 
}; 
  
// setting up rules for comparison 
// to sort the students based on names 
int comparator(const void* p, const void* q) 
{ 
    return strcmp(((struct Books*)p)->name, 
                  ((struct Books*)q)->name); 
} 
  
// Driver program 
int main() 
{ 
    int i = 0, n = 10; 
  
    struct Books arr[n]; 
  
    // Get the students data 
    arr[0].id = 1; 
    arr[0].name = "Mathematics-1"; 
    arr[0].price = 120; 
  
    arr[1].id = 2; 
    arr[1].name = "Mathematics-2"; 
    arr[1].price = 170; 
  
    arr[2].id = 3; 
    arr[2].name = "Mathematics-3"; 
    arr[2].price = 140; 
  
    arr[3].id = 4; 
    arr[3].name = "Chemistry"; 
    arr[3].price = 190; 
  
    arr[4].id = 5; 
    arr[4].name = "Physics"; 
    arr[4].price = 160; 
    
    arr[5].id = 6; 
    arr[5].name = "Biology"; 
    arr[5].price = 150; 
    
    arr[6].id = 7; 
    arr[6].name = "Engineering Drawing"; 
    arr[6].price = 120; 
    
    arr[7].id = 8; 
    arr[7].name = "Quantitative Aptitude"; 
    arr[7].price = 110; 
    
    arr[8].id = 9; 
    arr[8].name = "Political Science"; 
    arr[8].price = 120; 
    
    arr[9].id = 10; 
    arr[9].name = "Wren AND Martin"; 
    arr[9].price = 140; 
  
    // Print the Unsorted Structure 
    printf("Unsorted Books Records:\n"); 
    for (i = 0; i < n; i++) { 
        printf("Id = %d, Name = %s, Price = %d \n", 
               arr[i].id, arr[i].name, arr[i].price); 
    } 
    // Sort the structure 
    // based on the specified comparator 
    qsort(arr, n, sizeof(struct Books), comparator); 
  
    // Print the Sorted Structure 
    printf("\n\nBooks Records sorted by Name:\n"); 
    for (i = 0; i < n; i++) { 
        printf("Id = %d, Name = %s, Price = %d \n", 
               arr[i].id, arr[i].name, arr[i].price); 
    } 
  
    return 0; 
} 
