#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct student {
	int roll;
	char name[30];
	int std;
	// char subjects[6][20];
	// int marks[6];
	int marks;
}student_t;

void accept_student(student_t *s) {
	printf("roll: ");
	scanf("%d", &s->roll);
	printf("name: ");
	scanf("%s", s->name);
	printf("std: ");
	scanf("%d", &s->std);
	printf("marks: ");
	scanf("%d", &s->marks);
}
void display_student(student_t *s) {
	printf("%d, %s, %d, %d\n", s->roll, s->name, s->std, s->marks);
}

#define SIZE	5

typedef struct cirque {
	student_t arr[SIZE];
	int rear;
	int front;
	int count;
}cirque_t;

void cq_init(cirque_t *q) {
	memset(q->arr, 0, sizeof(q->arr));
	q->front = -1;
	q->rear = -1;
	q->count = 0;
}

void cq_push(cirque_t *q, student_t s) {
	q->rear = (q->rear+1) % SIZE;
	q->arr[q->rear] = s;
	q->count++;
}

void cq_pop(cirque_t *q) {
	q->front = (q->front+1) % SIZE;	
	q->count--;
}

student_t cq_peek(cirque_t *q) {
	int index = (q->front+1) % SIZE;
	return q->arr[index];
}

int cq_empty(cirque_t *q) {
	return q->count == 0;
}

int cq_full(cirque_t *q) {
	return q->count == SIZE;
}

int main() {
	cirque_t q;
	student_t temp;
	int choice;
	cq_init(&q);
	do {
		printf("\n0. exit\n1. push\n2. pop\n3. peek\nenter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // push
			if(cq_full(&q))
				printf("queue is full.\n");
			else {
				accept_student(&temp);
				cq_push(&q, temp);
			}
			break;
		case 2: // pop
			if(cq_empty(&q))
				printf("queue is empty.\n");
			else {
				temp = cq_peek(&q);
				cq_pop(&q);
				printf("popped: ");
				display_student(&temp);
			}
			break;
		case 3: // peek
			if(cq_empty(&q))
				printf("queue is empty.\n");
			else {
				temp = cq_peek(&q);
				printf("topmost: ");
				display_student(&temp);
			}
			break;
		}
	} while(choice != 0);
	return 0;
}

// terminal> gcc assign6.c -o assign6.exe
// terminal> assign6.exe


